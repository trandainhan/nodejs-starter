'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('addresses', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      value: Sequelize.STRING,
      state: Sequelize.STRING,
      source: Sequelize.STRING,
      coin: Sequelize.STRING,
      userId: Sequelize.BIGINT,
      genPrivId: Sequelize.BIGINT,
      managePubId: Sequelize.BIGINT,
      privId: Sequelize.BIGINT,
      pubId: Sequelize.BIGINT,
      data: Sequelize.JSONB,
      uuid: Sequelize.STRING,
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('addresses');
  }
};
