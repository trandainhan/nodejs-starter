import morgan from 'morgan';
import bodyParser from 'body-parser';
import { compact } from 'lodash';
import cors from 'cors';
import winston from './../../config/winston';

const loggerMiddleware = morgan('combined', { stream: winston.stream });
const corsMiddleware = cors();
const parseJsonMiddleware = bodyParser.json();

const middlewares = compact([
  process.env.NODE_ENV !== 'test' && loggerMiddleware,
  corsMiddleware,
  parseJsonMiddleware,
]);

export default middlewares;
