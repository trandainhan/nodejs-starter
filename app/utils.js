const catchError = (func, params, res) => {
  try {
    res.send(func(params));
  } catch(error){
    res.status(500).send(error.toString());
  }
}

const catchErrorAsync = async (func, params, res) => {
  try {
    res.send(await func(params));
  } catch(error){
    res.status(500).send(error.toString());
  }
}

export default {
  catchError,
  catchErrorAsync,
}
