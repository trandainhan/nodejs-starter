import chalk from 'chalk';
import figures from 'figures';

// eslint-disable-next-line no-console
export const log = console.log.bind(console);

// eslint-disable-next-line no-console
const info = messages => console.error(chalk.cyan(...[figures.info].concat(messages)));
// eslint-disable-next-line no-console
const success = messages => console.error(chalk.green(...[figures.tick].concat(messages)));
// eslint-disable-next-line no-console
const error = messages => console.error(chalk.red(...[figures.cross].concat(messages)));
// eslint-disable-next-line no-console
const warn = messages => console.error(chalk.yellow(...[figures.warning].concat(messages)));

export default {
  info,
  success,
  error,
  warn,
};
