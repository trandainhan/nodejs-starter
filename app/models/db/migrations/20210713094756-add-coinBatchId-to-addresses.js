'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn(
      'addresses',
      'coinBatchId',
      Sequelize.BIGINT,
    );
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.removeColumn(
      'addresses',
      'coinBatchId',
    );
  }
};
