FROM node:12

ARG github_commit_ref
ARG node_env=development

ENV GITHUB_COMMIT_REF $github_commit_ref
ENV NODE_ENV $node_env

WORKDIR /usr/src/app

COPY package*.json ./
COPY yarn.lock ./

RUN apt-get update && apt-get install -y libudev-dev libusb-1.0-0-dev
RUN yarn

COPY . .
COPY config/db/docker.js config/db/index.js

EXPOSE 3000

CMD [ "npm", "start" ]
