'use strict';

import _ from 'underscore';

module.exports = (sequelize, DataTypes) => {
  const BatchAddress = sequelize.define('batchaddresses', {
    coin: DataTypes.STRING,
    coinBatchId: DataTypes.BIGINT,
    size: DataTypes.INTEGER,
  });
  BatchAddress.single_name = 'BatchAddress';
  BatchAddress.associate = (models) => {
  };
  BatchAddress.prototype.toJson = function() {
    return _.pick(this, [ 'id', 'coin', 'coinBatchId', 'size' ]);
  }
  return BatchAddress;
};
