import TelegramBot from 'node-telegram-bot-api';
import config from 'config';


const getBotToken = () => config.get('TELEGRAM_BOT_TOKEN');

const getChatId = ({ name }) => {
  if (process.env.NODE_ENV != "production") { return config.get("TELEGRAM_GROUP_ID"); }
  return config.get(`TELEGRAM_${name.toUpperCase()}`);
}

const getBot = () => new TelegramBot(getBotToken(), {polling: true});

const sendMessage = async ({ groupName: name, message }) => {
  try {
    if (process.env.NODE_ENV == "test") { return; }
    const bot = getBot();
    const chatId = getChatId({ name });
    await bot.sendMessage(chatId, message);
  } catch (error) {
  }
}

export default {
  sendMessage,
}
