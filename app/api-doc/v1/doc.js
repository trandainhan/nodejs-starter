const apiDoc = {
  swagger: '2.0',
  basePath: '/api/v1',
  info: {
    title: 'Address API.',
    version: '1.0.0'
  },
  definitions: {
    ValidatingResponse: {
      type: 'object',
      properties: {
        check: {
          description: 'Check format address',
          type: 'boolean'
        }
      },
      required: ['check']
    },
    Address: {
      type: 'object',
      properties: {
        address: {
          type: 'string'
        }
      },
      required: ['address']
    },
    KeyPair: {
      type: 'object',
      properties: {
        privateKey: {
          type: 'string'
        },
        address: {
          type: 'string',
        }
      },
      required: ['privateKey', "address"],
    },
    BatchAddress: {
      type: 'object',
      properties: {
        id: {
          type: "integer"
        },
        coin: {
          type: "string"
        },
        coinBatchId: {
          type: "integer"
        },
        size: {
          type: "integer"
        }
      }
    },
    // BatchAddressRequest: {
    //   type: 'object',
    //   properties: {
    //     coin: {
    //       type: 'string'
    //     },
    //     batchSize: {
    //       type: 'integer'
    //     },
    //     options: {
    //       type: 'object',
    //       properties: {
    //         tokenStandard: {
    //           type: 'string'
    //         }
    //       }
    //     }
    //   },
    //   required: ["coin"],
    // },
    // BatchAddressResponse: {
    //   type: 'object',
    //   properties: {
    //     coin: {
    //       type: 'string'
    //     },
    //     coinBatchId: {
    //       type: 'string'
    //     },
    //     keyPairs: {
    //       type: 'array',
    //       items: {
    //         $ref: "#/definitions/KeyPair",
    //       }
    //     }
    //   },
    //   required: ["coin", "coinBatchId"],
    // },
    Transaction: {
      type: 'object',
      properties: {
        rawTx: {
          description: 'Raw transaction hex',
          type: 'string'
        }
      },
      required: ['rawTx']
    },
  },
  paths: {
    "/validateaddress/": {
      post: {
        tags: ["Address"],
        summary: "Validate address format",
        consumes: [ "application/json" ],
        produces: [ "application/json" ],
        parameters: [
          {
            name: "body",
            in: "body",
            required: true,
            type: "object",
            properties: {
              address: {
                type: 'string'
              },
              coin: {
                type: 'string'
              },
              options: {
                type: 'object',
                properties: {
                  tokenStandard: {
                    type: 'string'
                  }
                }
              }
            }
          },
        ],
        responses: {
          200: {
            description: "Successful operation",
            schema: {
              type: "object",
              properties: {
                status: {
                  type: 'string',
                },
                data: {
                  $ref: "#/definitions/ValidatingResponse",
                }
              }
            }
          }
        }
      }
    },

    "/addresses": {
      get: {
        tags: ["Address"],
        summary: "Get list of addresses by batch address",
        parameters: [
          {
            name: "coin",
            in: "query",
            type: "string",
            required: true,
          },
          {
            name: "coinBatchId",
            in: "query",
            type: "integer",
            required: true,
          },
        ],
        responses: {
          200: {
            description: "Successful operation",
            schema: {
              type: "object",
              properties: {
                status: {
                  type: 'string',
                },
                data: {
                  type: "object",
                  properties: {
                    addresses: {
                      type: 'array',
                      items: {
                        $ref: "#/definitions/Address",
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    },
    "/keypairs": {
      get: {
        tags: ["Key Pair"],
        summary: "Get list of key pairs by batch address",
        parameters: [
          {
            name: "coin",
            in: "query",
            type: "string",
            required: true,
          },
          {
            name: "coinBatchId",
            in: "query",
            type: "integer",
            required: true,
          },
        ],
        responses: {
          200: {
            schema: {
              type: "object",
              properties: {
                status: {
                  type: 'string',
                },
                data: {
                  type: "object",
                  properties: {
                    keyPairs: {
                      type: 'array',
                      items: {
                        $ref: "#/definitions/KeyPair",
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    },

    "/batchaddress": {
      get: {
        tags: ["Batch Address"],
        summary: "Get a batch address",
        parameters: [
          {
            name: "coin",
            in: "query",
            type: "string",
            required: true,
          },
          {
            name: "coinBatchId",
            in: "query",
            type: "integer",
            required: true,
          },
        ],
        responses: {
          200: {
            description: "Successful operation",
            schema: {
              type: "object",
              properties: {
                status: {
                  type: 'string',
                },
                data: {
                  type: "object",
                  properties: {
                    batchAddress: {
                      $ref: "#/definitions/BatchAddress",
                    }
                  }
                }
              }
            }
          }
        }
      },
      post: {
        tags: ["Batch Address"],
        summary: "Create a batch address",
        consumes: [ "application/json" ],
        produces: [ "application/json" ],
        parameters: [
          {
            name: "body",
            in: "body",
            required: true,
            schema: {
              type: "object",
              properties: {
                coin: {
                  type: "string"
                },
                options: {
                  type: "object",
                  properties: {
                    tokenStandard: {
                      type: "string",
                    }
                  }
                }
              }
            }
          },
        ],
        responses: {
          200: {
            description: "Successful operation",
            schema: {
              type: "object",
              properties: {
                status: {
                  type: 'string',
                },
                data: {
                  type: "object",
                  properties: {
                    batchAddress: {
                      $ref: "#/definitions/BatchAddress",
                    }
                  }
                }
              }
            }
          }
        }
      }
    },
    "/batchaddress/{batchAddressId}": {
      get: {
        tags: ["Batch Address"],
        summary: "Get batch address by id",
        consumes: [ "application/json" ],
        produces: [ "application/json" ],
        parameters: [
          {
            name: "batchAddressId",
            in: "path",
            required: true,
            type: "integer",
          },
        ],
        responses: {
          200: {
            description: "Successful operation",
            schema: {
              type: "object",
              properties: {
                status: {
                  type: 'string',
                },
                data: {
                  type: "object",
                  properties: {
                    batchAddress: {
                      $ref: "#/definitions/BatchAddress",
                    }
                  }
                }
              }
            }
          }
        }
      }
    },
    "/batchaddress/{batchAddressId}/addresses": {
      get: {
        tags: ["Batch Address"],
        summary: "Get list of addresses by batch address",
        consumes: [ "application/json" ],
        produces: [ "application/json" ],
        parameters: [
          {
            name: "batchAddressId",
            in: "path",
            required: true,
            type: "integer",
          },
        ],
        responses: {
          200: {
            description: "Successful operation",
            schema: {
              type: "object",
              properties: {
                status: {
                  type: 'string',
                },
                data: {
                  type: "object",
                  properties: {
                    addresses: {
                      type: 'array',
                      items: {
                        $ref: "#/definitions/Address",
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    },
    "/batchaddress/{batchAddressId}/keypairs": {
      get: {
        tags: ["Batch Address"],
        summary: "Get list of key pairs by batch address",
        consumes: [ "application/json" ],
        produces: [ "application/json" ],
        parameters: [
          {
            name: "batchAddressId",
            in: "path",
            required: true,
            type: "integer",
          },
        ],
        responses: {
          200: {
            schema: {
              type: "object",
              properties: {
                status: {
                  type: 'string',
                },
                data: {
                  type: "object",
                  properties: {
                    keyPairs: {
                      type: 'array',
                      items: {
                        $ref: "#/definitions/KeyPair",
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    },

    "/transaction": {
      post: {
        tags: ["Transaction"],
        summary: "Build raw transaction hex",
        consumes: [ "application/json" ],
        produces: [ "application/json" ],
        parameters: [
          {
            name: "body",
            in: "body",
            required: true,
            schema: {
              type: "object",
              properties: {
                coin: {
                  type: "string",
                },
                fromAddress: {
                  type: "string",
                },
                toAddress: {
                  type: "string",
                },
                amount: {
                  type: "string",
                },
                options: {
                  type: "object",
                  properties: {
                    tokenStandard: {
                      type: "string"
                    }
                  }
                }
              }
            }
          },
        ],
        responses: {
          200: {
            schema: {
              type: "object",
              properties: {
                status: {
                  type: 'string',
                },
                data: {
                  type: "object",
                  properties: {
                    rawTx: {
                      type: "string"
                    }
                  }
                }
              }
            }
          }
        }
      }
    },
  },
  tags: [
    {
      name: "Address",
      description: "",
    },
    {
      name: "Key Pair",
      description: "",
    },
    {
      name: "Batch Address",
      description: "",
    },
    {
      name: "Transaction",
      description: "",
    }
  ]
};

export default apiDoc;
