import models from '.';

const sequelize = models.sequelize;
const { Address, Ckey, BatchAddress } = models;

describe('Models: BatchAddress', () => {
  beforeEach(models.truncatedb);

  it('success', async () => {
    const coin = "ETH";
    const t = await sequelize.transaction();
    try {
      const batchAddress = await BatchAddress.create({ coin, coinBatchId: 0, size: 1 }, { transaction: t });

      const pubKey = await Ckey.create({ name: 'publicKey', value: 'value1' }, { transaction: t });
      const privKey = await Ckey.create({ name: 'privateKey', value: 'value2' }, { transaction: t });

      const address = await Address.create({
        value: "address",
        state: "state",
        source: "source",
        coin,
        userId: 0,
        privId: pubKey.id,
        pubId: privKey.id,
        batchAddressId: batchAddress.id,
        uuid: 1,
      }, { transaction: t });

      await t.commit();
      expect(await BatchAddress.count()).toEqual(1);
      expect(await Address.count()).toEqual(1);
      expect(await Ckey.count()).toEqual(2);
    } catch (error) {
      await t.rollback();
      expect(await BatchAddress.count()).toEqual(0);
      expect(await Address.count()).toEqual(0);
      expect(await Ckey.count()).toEqual(0);
    }
  })
})
