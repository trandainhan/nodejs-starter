import compact from 'lodash/fp/compact';
import appRoot from 'app-root-path';
import winston from 'winston';
import Sentry from'winston-sentry';
import config from 'config';

import sentryConfig from './sentry';

const { createLogger, format, transports } = winston;
const { combine, timestamp, prettyPrint } = format;

const options = {
  errorLog: {
    level: 'error',
    filename: `${appRoot}/logs/app.error.log`,
    handleExceptions: true,
    json: true,
    maxsize: 5 * 1000 * 1000,
    maxFiles: 5,
    colorize: false,
  },
  infoLog: {
    level: 'info',
    filename: `${appRoot}/logs/app.info.log`,
    handleExceptions: true,
    json: true,
    maxsize: 5 * 1000 * 1000,
    maxFiles: 5,
    colorize: false,
  },
  console: {
    level: 'debug',
    handleExceptions: true,
    json: false,
    colorize: true,
  },
  sentry: {
    level: 'error',
    dsn: config.get('SENTRY_DNS'),
    ...sentryConfig,
  },
};

const logger = createLogger({
  format: combine(
    timestamp(),
    prettyPrint(),
  ),
  transports: compact([
    new transports.File(options.errorLog),
    new transports.File(options.infoLog),
    new transports.Console(options.console),
    process.env.NODE_ENV !== 'test' && new Sentry(options.sentry),
  ]),
  exitOnError: false,
});

// eslint-disable-next-line immutable/no-mutation
logger.stream = {
  write: (message) => {
    logger.info(message);
  },
};

// usage
// winston.info('Hello again distributed logs');

export default logger;
