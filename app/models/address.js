'use strict';

import _ from 'underscore';

module.exports = (sequelize, DataTypes) => {
  const Address = sequelize.define('addresses', {
    value: DataTypes.STRING,
    state: DataTypes.STRING,
    source: DataTypes.STRING,
    coin: DataTypes.STRING,
    userId: DataTypes.BIGINT,
    genPrivId: DataTypes.BIGINT,
    managePubId: DataTypes.BIGINT,
    privId: DataTypes.BIGINT,
    pubId: DataTypes.BIGINT,
    coinBatchId: DataTypes.BIGINT,
    data: DataTypes.JSONB,
    uuid: DataTypes.STRING,
  });
  Address.single_name = 'Address';
  Address.associate = (models) => {
    const { Address, Ckey, BatchAddress } = models;
    Address.belongsTo(Ckey, {
      foreignKey: 'privId',
      allowNull: true,
    });
    Address.belongsTo(Ckey, {
      foreignKey: 'pubId',
      allowNull: true,
    });
  };
  Address.prototype.toJson = function() {
    return _.pick(this, [ 'value', 'coin', 'coinBatchId' ]);
  }
  Address.prototype.toSimpleJson = function() {
    return { address: this.value };
  }
  return Address;
};
