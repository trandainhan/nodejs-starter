import { StatusCodes } from 'http-status-codes';

import Services from './../services';

const { AddressService, BatchAddressService } = Services;

module.exports = (cb) => {
  let controller = {};

  controller.validateAddress = async (req, res) => {
    try {
      const { coin, address, options } = req.body;
      let data, statusCode, error;
        cb({
            res,
            data: AddressService.validate({ coin, address, options }),
            statusCode: StatusCodes.OK,
        });
    } catch (error) {
      cb({
        res,
        statusCode: StatusCodes.INTERNAL_SERVER_ERROR,
        error: error
      })
    }
  }
  return controller;
}
