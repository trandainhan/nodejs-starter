import {
  StatusCodes,
  getReasonPhrase,
  getStatusCode,
} from 'http-status-codes';
import { forEach } from 'lodash/fp';

import winston from './../../config/winston';

const responseHandler = async ({ res, data, statusCode, error }) => {
  let respData;
  if (error) {
    const errorMsg = error.toString();
    respData = { success: false, error: errorMsg };
    winston.error(error);
  } else {
    respData = { success: true, data };
  }
  res.status(statusCode).json(respData);
}

const controllers = {
  AddressController: require("./address")(responseHandler),
}

export default controllers;
