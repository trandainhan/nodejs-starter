module.exports = {
  name: process.env.NODE_ENV,
  NODE_ENV: process.env.NODE_ENV,
  CKEY_SECRET: "0cff0df1e8a5ce32be5d40d2d49eb131b6e907ca2cdeab251f83f1b9dbd44c248f6a2f97c8e9f61bcd5a65bc43cc34fbc933244d1b2e386c6c30aecef1c71e68",
  TELEGRAM_BOT_TOKEN: "",
  TELEGRAM_GROUP_ID: "",
  SENTRY_DNS: "",
}
