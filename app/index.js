import express from 'express';
import morgan from 'morgan';
import config from 'config';
import { forEach } from 'lodash/fp';

import swaggerUi from 'swagger-ui-express';
import v1ApiDoc from './api-doc/v1/doc';

import middlewares from './middlewares';

import routes from './routes';

const app = express();

forEach(middleware => app.use(middleware), middlewares);

app.use('/api-docs/v1', swaggerUi.serve, swaggerUi.setup(v1ApiDoc));

app.get('/health', (req, res) => {
  res.send({
    timestamp: Date.now(),
    ref: process.env.GITHUB_COMMIT_REF || null,
    environment: process.env.NODE_ENV || 'development',
  })
})

app.use('/api/v1/addresses', routes.addressesRoute);

module.exports = app
