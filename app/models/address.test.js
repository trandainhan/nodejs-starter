import models from '.';

const sequelize = models.sequelize;
const { Address, Ckey } = models;

describe('Models: Address', () => {
  beforeEach(models.truncatedb);

  describe('#create', () => {
    it('success', async () => {
      const t = await sequelize.transaction();
      try {
        const pubKey = await Ckey.create({ name: 'publicKey', value: 'value1' }, { transaction: t });
        const privKey = await Ckey.create({ name: 'privateKey', value: 'value2' }, { transaction: t });

        const address = await Address.create({
          value: "address",
          state: "state",
          source: "source",
          coin: "coin",
          userId: 0,
          privId: pubKey.id,
          pubId: privKey.id,
          // uuid: "uuid",
        }, { transaction: t });

        await t.commit();
        expect(await Address.count()).toEqual(1);
        expect(await Ckey.count()).toEqual(2);
      } catch (error) {
        await t.rollback();
        expect(await Address.count()).toEqual(0);
        expect(await Ckey.count()).toEqual(0);
      }
    })
  })

  describe('#toJson', () => {
    let address;
    beforeEach(async () => {
      const t = await sequelize.transaction();
      try {
        const pubKey = await Ckey.create({ name: 'publicKey', value: 'value1' }, { transaction: t });
        const privKey = await Ckey.create({ name: 'privateKey', value: 'value2' }, { transaction: t });

        address = await Address.create({
          value: "address",
          state: "state",
          source: "source",
          coin: "coin",
          userId: 0,
          privId: pubKey.id,
          pubId: privKey.id,
          // uuid: "uuid",
        }, { transaction: t });

        await t.commit();
        expect(await Address.count()).toEqual(1);
        expect(await Ckey.count()).toEqual(2);
      } catch (error) {
        await t.rollback();
        expect(await Address.count()).toEqual(0);
        expect(await Ckey.count()).toEqual(0);
      }
    })

    it('success', async () => {
      expect(Object.keys(address.toJson())).toEqual([ 'value', 'coin', 'coinBatchId' ]);
    })
  })
})
