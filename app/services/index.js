// import SERVICES from './coin-services';
//
// const sign = ({ coin, params, privateKey }) => SERVICES[coin].signer.sign({ params, privateKey });
// const sign_async = async ({ coin, params, privateKey, options }) => await SERVICES[coin].signer.sign_async({ params, privateKey, options });
//
// const generateAddress = ({ coin, count }) => SERVICES[coin].wallet.generateAddress(count);
//
// const validateAddress = ({ coin, address }) => SERVICES[coin].wallet.validateAddress({ address });

// export default {
  // sign,
  // sign_async,
  // generateAddress,
  // validateAddress,
// };


import AddressService from "./address_service";
// import CoinService from "./coin-service";
import BatchAddressService from "./batch_address_service";
import TransactionService from "./transaction_service";
import NotificationService from './notification_service';

export default {
  AddressService,
  BatchAddressService,
  // CoinService,
  TransactionService,
  NotificationService,
};
