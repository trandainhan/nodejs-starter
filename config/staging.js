module.exports = {
  name: process.env.NODE_ENV,
  NODE_ENV: process.env.NODE_ENV,
  CKEY_SECRET: process.env.CKEY_SECRET,
  TELEGRAM_BOT_TOKEN: process.env.TELEGRAM_BOT_TOKEN,
  TELEGRAM_GROUP_ID: process.env.TELEGRAM_GROUP_ID,
  SENTRY_DNS: process.env.SENTRY_DNS,
}
