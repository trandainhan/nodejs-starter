import controllers from './../controllers';

const { AddressController } = controllers;

const router = require('express').Router();

router.get('/', AddressController.validateAddress);

export default router
