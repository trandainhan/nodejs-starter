module.exports = {
  release: process.env.GITHUB_COMMIT_REF,
  environment: process.env.NODE_ENV,
};
